function problem4(inventory){
    let carYear = [];
    for (let year of inventory){
        carYear.push(year.car_year);
    }
    return carYear
}

export default problem4;