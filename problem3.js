function problem3(inventory){
    let list =[];
    for (let carmodel of inventory){
        list.push(carmodel.car_model);
    }
    const sortedList= list.sort()
    return sortedList; 
}

export default problem3;