function problem6(inventory){
    let BMWAndAUDI = [];
    for (let carInfo of inventory){
        if (carInfo.car_make === "BMW" || carInfo.car_make === "Audi"){
            BMWAndAUDI.push(carInfo.car_make);
        }
    }
    return JSON.stringify(BMWAndAUDI);
}

export default problem6;