function problem2(inventory){
    const lastcar = inventory.length-1;
    return `Last car is a ${inventory[lastcar].car_make} ${inventory[lastcar].car_model} `;
}

export default problem2