function problem1(inventory){
    for (let carInfo of inventory){
        if (carInfo.id===33){
            return(`Car 33 is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`);
        }
    }
}

export default problem1;